package model

/**
  * Created by d_rc on 23/01/2017.
  */
case class Clause(literals: Map[Int, Literal])
