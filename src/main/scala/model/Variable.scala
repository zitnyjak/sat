package model

/**
  * Created by d_rc on 23/01/2017.
  */
case class Variable (id: Int, weight: Double)
