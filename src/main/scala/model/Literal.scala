package model

/**
  * Created by d_rc on 23/01/2017.
  */

case class Literal (present: Boolean = false,
                    weight: Double = 0.5,
                    negated: Boolean = false)
