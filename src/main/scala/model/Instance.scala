package model

import util.InstanceGenerator

/**
  * Created by d_rc on 23/01/2017.
  */
case class Instance (variables: List[Variable], clauses: List[Clause]) {

  def evaluateLiteral(literal: Literal, value: Boolean) =
    if (literal.negated) !value else value

  def evaluateClause(clause: Clause, variableValues: List[Boolean]): Boolean = {
    var i: Int = 0
    for ((variableId, literal) <- clause.literals) {
      val literalResult = evaluateLiteral(literal, variableValues(variableId))
      if (literalResult) {
        return true
      }
      i += 1
    }

    false
  }

  def evaluate(variableValues: List[Boolean]): Boolean = {
    var i = 0
    for (clause <- clauses) {
      val clauseResult = evaluateClause(clause, variableValues)
      if (!clauseResult) {
        return false
      }
      i += 1
    }

    true
  }

  def satisfiedClauses(variableValues: List[Boolean]): Int = {
    var satisfiedClauses = 0
    for (clause <- clauses) {
      val clauseResult = evaluateClause(clause, variableValues)
      if (clauseResult) {
        satisfiedClauses += 1
      }
    }
    return satisfiedClauses
  }

  // TODO: better struct, better functional
  def calculateSolutionValue(variableValues: List[Boolean]): Double = {
    var i = 0
    var solValue = 0.0
    for (variableValue <- variableValues) {
      if (variableValue) {
        solValue += variables(i).weight
      }
      i += 1
    }
    solValue
  }

  override def toString: String = {
    var stringRepresentation = s"p cnf $variables ${clauses.size}\n"
    for (clause <- clauses) {
      for ((i, literal) <- clause.literals) {
        if (literal.present) {
          val neg = if (literal.negated) "-" else ""
          stringRepresentation += s"$neg${i+1} "
        }
      }
      stringRepresentation += "0\n"
    }

    stringRepresentation
  }
}

object Instance {
  def random() = InstanceGenerator.generateInstance()
}
