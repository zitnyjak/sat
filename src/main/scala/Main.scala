import solvers.{DummySolver, GeneticSolver}
import util.{InstanceGenerator, InstanceReader, InstanceWriter, Timer}

import scala.io.Source
import scala.io.StdIn._

object Main {

  def main(args: Array[String]): Unit = {
    var varCount = 4
    var clauseCount = 6
    var evolCount = 20
    var popSize = 20
    var cr = 0.2
    var mr = 10
    if (args.length == 2) {
      varCount = args(0).toInt
      clauseCount = args(1).toInt
    }

    var errs: Map[Int, Double] = Map()
    var errMaxs: Map[Int, Double] = Map()

    //List.range(5, 15).foreach(i => {
    var localErrs: List[Double] = List()
    List.range(1, 100).foreach(j => {
      val instance = InstanceGenerator.generateInstance(varCount, clauseCount)

//        val resultTuple = Timer.measureDuration {
//          // DummySolver.isSatisfiable(instance)
//          val dummySolver = new DummySolver
//          dummySolver.bestValues(instance)
//        }("isSatisfiable")
//        // println(xs"Dummy: (${resultTuple._1._1}) -> ${resultTuple._1._2} in ${resultTuple._2} ms")
//        // if(resultTuple._1._2 != List())
//        // println(s"Check: ${instance.evaluate(resultTuple._1._2)}")

      val resultTuple2 = Timer.measureDuration {
          // DummySolver.isSatisfiable(instance)
          // DummySolver.bestValues(instance)
        val geneticSolver = new GeneticSolver(100, 20, cr, mr)
        geneticSolver.bestValues(instance)
      }("isSatisfiable")
        println(s"Genetic: (${resultTuple2._1._1}) -> ${resultTuple2._1._2} in ${resultTuple2._2} ms")
//        val err = Math.abs(resultTuple2._1._1 - resultTuple._1._1)
//        localErrs = List.concat(localErrs, List(err))
//         println(s"Error: $err")
        if (resultTuple2._1._1 != 0.0)
          println(s"Check: ${instance.evaluate(resultTuple2._1._2)}")
      })
//      val errMean = localErrs.sum / localErrs.size
//      val errMax = localErrs.max
//      errs = errs + (i -> errMean)
//      errMaxs = errMaxs + (i -> errMax)
    //})

//    println(errs.toList.sortBy(_._1).map(_._2).mkString("\n"))
    // println(errMaxs)

//    val instance = InstanceGenerator.generateInstance(varCount, clauseCount)
//    // val aimInstancePath = "/Users/d_rc/Downloads/aim/aim-100-1_6-no-1.cnf"
//    // val aimInstancePath = "/Users/d_rc/Downloads/aim/aim-100-1_6-yes1-2.cnf"
//    // val aimInstancePath = "/Users/d_rc/Downloads/aim/dimacs_sample.cnf"
//    // val instance = InstanceReader.readFile(aimInstancePath)
//
//    // InstanceWriter.print2(instance)
//    // println(instance)
//
//    val resultTuple = Timer.measureDuration {
//      // DummySolver.isSatisfiable(instance)
//      DummySolver.bestValues(instance)
//    }("isSatisfiable")
//    println(s"Dummy: (${resultTuple._1._1}) -> ${resultTuple._1._2} in ${resultTuple._2} ms")
//    if(resultTuple._1._2 != List())
//      println(s"Check: ${instance.evaluate(resultTuple._1._2)}")
//
//    val resultTuple2 = Timer.measureDuration {
//      // DummySolver.isSatisfiable(instance)
//      // DummySolver.bestValues(instance)
//      GeneticSolver.bestValues(instance)
//    }("isSatisfiable")
//    println(s"Genetic: (${resultTuple2._1._1}) -> ${resultTuple2._1._2} in ${resultTuple2._2} ms")
//    val err = Math.abs(resultTuple2._1._1 - resultTuple._1._1)
//    println(s"Error: $err")
//    println(s"Check: ${instance.evaluate(resultTuple2._1._2)}")

    // InstanceWriter.writeFile(instance, "/tmp/asd")

    // NOTEs
    // + or, x and


    // TODOs
    // - parametrize weights, consts
    // - dummy solve, genetic solve
    // - genetic evolve till true
    // - save if sat -> find best on trues
    // - errors, stats

    // TODOs
    // - fitness MAX clauses
    // - ratio, errors, stats
    // - genetika
    // - trap

  }
}