package solvers
import model.Instance
import org.jgap.impl.BooleanGene
import org.jgap.{Chromosome, Configuration, Gene, Genotype}



class GeneticSolver(var evolutionCount: Int = 0,
                    var populationSize: Int = 0,
                    var crossoverRate: Double = 0.0,
                    var mutationRate: Int = 0) extends Solver {

  val EvolutionCount = 100
  val PopulationSize = 100
  val CrossoverRate = .2
  val MutationRate = 10
  val Evolve = false


  if (evolutionCount == 0) {
    evolutionCount = EvolutionCount
  }

  if (populationSize == 0) {
    populationSize = PopulationSize
  }

  if (crossoverRate == 0) {
    crossoverRate = CrossoverRate
  }

  if (mutationRate == 0) {
    mutationRate = MutationRate
  }

  override def isSatisfiable(instance: Instance): Boolean = ???


  // TODO love this
  def constructBestSolution(population: Genotype, instance: Instance): List[Boolean] = {
    val bestSolutionSoFar = population.getFittestChromosome()

    val bitLength = Math.pow(2, instance.variables.size).toInt - 1
    var bestSol = List.range(0, instance.variables.size).map(_ => false)
    var id = 0
    bestSolutionSoFar.getGenes.foreach {
      case gene: BooleanGene =>
        val present = gene.getAllele.asInstanceOf[Boolean]
        if (present) {
          bestSol = bestSol.patch(id, List(present), 1)
        }
        id += 1
      case _ => throw new ClassCastException
    }

    bestSol
  }

  override def bestValues(instance: Instance): (Double, List[Boolean]) = {
    Configuration.reset()

    val conf = new CustomGAConfiguration(crossoverRate, mutationRate)
    conf.setPreservFittestIndividual(true)
    val func = new SATFitnessFunction(instance)
    conf.setFitnessFunction(func)

    val genes: List[Gene] = List.range(0, instance.variables.size).map(_ => {
      new BooleanGene(conf, true)
    })

    val sampleChromosome = new Chromosome(conf, genes.toArray)
    conf.setSampleChromosome(sampleChromosome)
    conf.setPopulationSize(populationSize)

    var bestSolution: List[Boolean] = List()
    var bestSolutionValue: Double = 0.0
    var i = 1
    while (true) {
      val population = Genotype.randomInitialGenotype(conf)
      population.evolve(evolutionCount)

      bestSolution = constructBestSolution(population, instance)
      bestSolutionValue = Math.round(instance.calculateSolutionValue(bestSolution) * 100) / 100.0
      val evald = instance.evaluate(bestSolution)
      if (!Evolve || evald) {
        if (!evald) return (0, List())
        return (bestSolutionValue, bestSolution)
      } else {
        val epoch = evolutionCount * i
        val satisfiedClauses = instance.satisfiedClauses(bestSolution)
        println(s"Nothing in ${epoch} epochs. Most clauses = ${satisfiedClauses}, best value = ${bestSolutionValue}")
      }
      i += 1
    }

    (bestSolutionValue, bestSolution)
  }

}
