package solvers

import model.Instance
import org.jgap.{FitnessFunction, IChromosome}

/**
  * Construct the FitnessFunction class for JGAP.
  */
class SATFitnessFunction(instance: Instance) extends FitnessFunction {
  val MAX_BOUND = 1000000000.0d

  def evaluate(chromosome: IChromosome): Double = {
    // calc value
    // TODO chromos to bitList
    val geneList = chromosome.getGenes.toList
    val variableValues = geneList.map(gene => {
      gene.getAllele.asInstanceOf[Boolean]
    })

    // TODO check if sats, different values when false !!
    val satisfies = instance.evaluate(variableValues)
    if (!satisfies) {
      val satisfiedClauses = instance.satisfiedClauses(variableValues)
      // println(satisfiedClauses)
      return satisfiedClauses
    }


    val solutionValue = instance.calculateSolutionValue(variableValues)
    // println(instance.clauses.size + solutionValue)
    // Math.max(1.0d, Math.min(solutionValue, 0.5))
    instance.clauses.size + solutionValue
  }

}
