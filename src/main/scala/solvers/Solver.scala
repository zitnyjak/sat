package solvers

import model.Instance

/**
  * Created by d_rc on 23/01/2017.
  */
trait Solver {
  def isSatisfiable(instance: Instance): Boolean
  def bestValues(instance: Instance): (Double, List[Boolean])
}
