package solvers

import model.{Clause, Instance}
import util.InstanceGenerator

/**
  * Created by d_rc on 23/01/2017.
  */
class DummySolver extends Solver {

  val Debug = false
  var solutions: List[List[Boolean]] = List()

  def getSolutions(variableCount: Int) = {
    if (solutions.isEmpty) {
      solutions = generateAllPermutations(variableCount)
    }

    solutions
  }

  def generateAllPermutations(variableCount: Int): List[List[Boolean]] = {
    val max = Math.pow(2, variableCount).toInt

    // print(s"generating $max solutions..")
    val binStrings = List.range(0, max).map(_.toBinaryString)

    // println(binStrings.mkString("\n"))

    if (Debug) print(".")
    val permutations: List[List[Boolean]] = binStrings.map(binString => {
      var binList = binString.toList.map(_ - '0' != 0)
      if (Debug) print(".")

      val maxLength = (max - 1).toBinaryString.length
      if (binList.size < maxLength) {
        val prependixLength = maxLength - binList.size
        val prependix: List[Boolean] = List.range(0, prependixLength).map(x => false)
        // println(s"concatting ${prependix} to $binList")
        binList = prependix ++ binList //!!
        // println(binList)
      }

      binList
    })
    if (Debug) println()
    permutations
  }

  def isSatisfiable(instance: Instance): Boolean = {
    val solutions = getSolutions(instance.variables.size)
    // println(s"trying ${solutions.size} solutions..")
    for (solution <- solutions) {
      val solutionSatisfiable = instance.evaluate(solution)
      if (solutionSatisfiable) {
        return true
      }
    }
    false
  }

  override def bestValues(instance: Instance): (Double, List[Boolean]) = {
    if (!isSatisfiable(instance)) return (0, List())

    val solutions = getSolutions(instance.variables.size)
    val trueSolutions = solutions.filter(solution => {
      instance.evaluate(solution)
    })

//    println("=======")
//    println(solutions.mkString("\n"))
//    println("=======")
//    println(trueSolutions.mkString("\n"))

    var bestSolutionValue = 0.0
    var bestSolution: List[Boolean] = Nil
    for (solution <- trueSolutions) {
      val solValue = instance.calculateSolutionValue(solution)
      if (solValue > bestSolutionValue) {
        bestSolutionValue = solValue
        bestSolution = solution
      }
    }

    (Math.round(bestSolutionValue * 100) / 100.0, bestSolution)
  }
}
