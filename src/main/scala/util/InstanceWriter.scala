package util

import java.io.{BufferedWriter, File, FileWriter}

import model.Instance

/**
  * Created by d_rc on 23/01/2017.
  */
object InstanceWriter {
  def writeFile(instance: Instance, outputFile: String): Unit = {
    val file = new File(outputFile)
    val bw = new BufferedWriter(new FileWriter(file))
    bw.write("c this is a comment\n")
    bw.write(instance.toString)
    bw.close()
  }

  def print1(instance: Instance): Unit = {
    for (clause <- instance.clauses) {
      for ((i, literal) <- clause.literals) {
        if (literal.present) {
          val neg = if (literal.negated) "'" else ""
          print(s"x$i$neg(${literal.weight}) ")
        }
      }
      println()
    }
  }

  def print2(instance: Instance): Unit = {
    println(s"p cnf ${instance.variables} ${instance.clauses.size}")
    for (clause <- instance.clauses) {
      for ((i, literal) <- clause.literals) {
        if (literal.present) {
          val neg = if (literal.negated) "-" else ""
          print(s"$neg${i+1} ")
        }
      }
      print("0")
      println()
    }
  }
}
