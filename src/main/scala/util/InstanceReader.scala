package util

import model.{Clause, Instance, Literal, Variable}

import scala.io.Source
import scala.collection.mutable

/**
  * Created by d_rc on 23/01/2017.
  */
object InstanceReader {

  def readFile(inputFile: String): Instance = {
    var variableCount: Int = 0
    var variables: List[Variable] = List()
    var clauseCount: Int = 0
    var clauses: Array[Clause] = Array()

    var clauseNo = 0
    for (line <- Source.fromFile(inputFile).getLines()) {
      if (line(0) == 'p') {
        val lineParts = line.split(" ")
        variableCount = lineParts(2).toInt
        variables = List.range(0, variableCount).map(varId => {
          Variable(varId, InstanceGenerator.generateWeight())
        })
        clauseCount = lineParts(3).toInt
      } else if (line(0) != 'c') {
        val lineData = line.split(" ")
        var literals: Map[Int, Literal] = Map()
        for (lineLiteral <- lineData if lineLiteral.toInt != 0) {
          val lineLiteralAsNumber = lineLiteral.toInt
          val variableNumber = Math.abs(lineLiteralAsNumber)
          val weight = InstanceGenerator.generateWeight()
          val negated = lineLiteralAsNumber < 0

          literals = literals + (variableNumber - 1 -> Literal(present = true, weight, negated))

        }
        clauses = clauses :+ Clause(literals)
        clauseNo += 1
      }
    }

    val clausesAsList = clauses.toList

    Instance(variables, clausesAsList)
  }

}
