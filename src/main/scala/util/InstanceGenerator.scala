package util

import scala.util.Random
import model.{Clause, Instance, Literal, Variable}

/**
  * Created by d_rc on 23/01/2017.
  */
object InstanceGenerator {
  val VarRange = Range(-9, 10)

  val VariablesCount = 4
  val ClauseCount = 6

  val SignProbability = 0.5
  val PresentProbability = 0.9

  val NSat = 3

  def generateWeight(): Double = Math.round(Math.random * 100) / 100.0

  def generateLiteral(): Literal = {
    val weight = generateWeight()
    val sign: Boolean = math.random < SignProbability
    val present: Boolean = math.random < PresentProbability

    Literal(present, weight, sign)
  }

  def generateClause(variables: Int): List[Literal] = {
    val literals: List[Literal] = List.range(0, variables).map(i => {
      generateLiteral()
    })

    literals
  }

  def generateClauseNSat(variables: Int, n: Int = NSat): Clause = {
    val nVars = Random.shuffle(List.range(0, variables)).slice(0, n)
    val literalTuple = nVars.map(nVar => {
      (nVar, generateLiteral())
    })

    Clause(literalTuple.toMap)
  }

  def generateClauses(variables: Int, clauseCount: Int): List[Clause] = {
    val clauses: List[Clause] = List.range(0, clauseCount).map(i => {
      generateClauseNSat(variables)
    })

    clauses
  }

  def generateVariables(variableCount: Int): List[Variable] = {
    val variables = List.range(0, variableCount).map(varId => {
      val weight = generateWeight()
      Variable(varId, weight)
    })
    variables
  }

  def generateInstance(variableCount:Int = VariablesCount,
                       clauseCount: Int = ClauseCount) = {
    val variables = generateVariables(variableCount)
    val clauses = generateClauses(variableCount, clauseCount)
    Instance(variables, clauses)
  }

  def generateInstances(n: Int): List[Instance] = {
    val instances: List[Instance] = List.range(0, n).map(i => {
      generateInstance()
    })

    instances
  }
}
